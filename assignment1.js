// Reference link for the questions :  https://www.codingame.com/playgrounds/9799/learn-solve-call-apply-and-bind-methods-in-javascript

// Q1
var func = function() {
  console.log(this)
}.bind(8)

func();// output will be [Number: 8]

// Q2
var func = function() {
  console.log(this)
}.bind(1);

var obj = {
  callFun : func
}
obj.callFun.func(); // output will be type error 
obj.callFun(); // output will be correct here [Number: 8]

// Q3
function checkFun(a,b,c){
  console.log(this);
  console.log(a);
  console.log(b);
  console.log(c);
}
checkFun.call(1,2,3,4); //output [String: 'm'] 2 3 4


// Q4
function checkFun(a, b, c){
  console.log(this);
  console.log(a);
  console.log(b);
  console.log(c);
}
checkFun.apply(1,[2,3,4]); //  output will be same as above Q3

// Q5
function sumOfNumbers() {
  var total = 0;
  for(var i = 0; i < arguments.length; i++){
      total += arguments[i];
  }
  return total;
}
var sum = sumOfNumbers(1,2,3);
console.log(sum); // output is 6

// Q6
function sumOfNumbers() {
  var total = 0;
  for(var i = 0; i < arguments.length; i++){
      total += arguments[i];
  }
  return total;
}
var sum = sumOfNumbers.call(null,1,2,3);
console.log(sum); // output 6

// Q7
function sumOfNumbers() {
  var total = 0;
  for(var i = 0; i < arguments.length; i++){
      total += arguments[i];
  }
  return total;
}
var numbers = [1,2,3];
var sum = sumOfNumbers.apply(null,numbers); // yaha null k jgh 0 bhi pass kr skte h 
console.log(sum); //output is 6


// Q8
function updateZipCode() {
  console.log(this)
}
updateZipCode.call(1) //output [Number: 1]

// Q9
var updateZipCode = function () {
  console.log(this);
};
updateZipCode.call({}); //output will  be empty object - {}

// Q10
var updateZipCode = function () {
  console.log(this);
};
updateZipCode.call({ zip: '11787'}); //output will be - { zip: '11787' }

// Q11
var updateZipCode = function () {
  console.log(this);
};
var zipCode = {
  zip: '11787'
};
updateZipCode.call(zipCode); //output is { zip: '11787' }


// Q12
var updateZipCode = function (newZip, country) {
  console.log(newZip + ' ' + country);
};
var zipCode = {
  zip: '11787'
};
updateZipCode.call(zipCode, '11888', 'us');// output is 11888 us
updateZipCode(19839,'ms') // 19839 ms

// Q13
var updateZipCode = function (newZip, country) {
  console.log(newZip + ' ' + country);
};
var zipCode = {
  zip: '11787'
};
updateZipCode.apply(zipCode, ['11888', 'us']);// output is 11888 us

// Q14
"use strict";
var zipcode = {
    checkZipcode : function() {
        console.log(this);
        function updateZipCode() {
            console.log(this);
        }
        updateZipCode.call(this);
    }
}
zipcode.checkZipcode(); // output is { checkZipcode: [Function: checkZipcode] }{ checkZipcode: [Function: checkZipcode] }


// Q15
"use strict";
var zipcode = {
    checkZipcode : function() {
        console.log(this);
        var updateZipCode = function() {
            console.log(this);
        }.bind(this);
        updateZipCode();
    }
}
zipcode.checkZipcode(); //output is { checkZipcode: [Function: checkZipcode] }{ checkZipcode: [Function: checkZipcode] }

// Q16
"use strict";
var person = {
    name : "Jack",
    prop : {
        name : "Daniel",
        getName : function() {
            return this.name;
        }
    }
}

var name = person.prop.getName.bind(person);
console.log(name()); //output is Jack

var  name = person.prop.getName();
console.log(name); //output is Daniel
