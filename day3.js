// First Class Function

// function statement - when a function is defined in below format
// it is also known as function declaration
function a(){
  console.log("Function statement")
} // here hoisting can be done

// function expression - when function is assigned to a variable. here function acts like a value.
var b = function(){
  console.log("function expresion")
} // hoisting cannot be done


// anonymous function - function without a name and it does nopt have their own identity
//anonymous functions are used when functions are used as values
function(){

}// it will be syntax error


// named function expression 
var c = function xyz(){
  console.log("named function expression")
}
c()//coreect output
xyz() // will give reference error

// first class function - ability to use functions as value in known as first class function
 var d = function abc(){
  return function xyz(){

  }
 }
console.log(d())

//regular and arrow function
//arrow functions are introduced in ES6



// 1 - no keyword argument is present in arrow function for arguments
function findMaxNumber() {
  console.log(arguments)
  return Math.max(...arguments)
}
console.log(findMaxNumber(5, 10, 3, 20, 100))

const arrowFun = () => {
  console.log(arguments)
} // reference error 
arrowFun(5, 10, 3, 20, 100)

// 2 - no prototype object for arrow function

function nrml(){

}
const arr = () => {

}

console.log(nrml.prototype)
console.log(arr.proto) // undefined 

// 3 - Cannot invoked using "new" keyword

const UserDetails = (name) => {
  this.name = name
}
const user1 = new UserDetails("Muskan") // output is typer error

// 4- no duplicate params can be passed
var sum = (a,b) =>{
  return a + b
}
console.log(sum(2,3))

var sum = function(a, a){
  return a + a
}
console.log(sum(2,3)) // output 6

var sum = (a,a) =>{
  return a + a
}
console.log(sum(2,3)) // syntax error