// let num1 = 1
// let num2 = 2 
// console.log("hello", num1+num2)

// let num1 = 8
// let num2 = 6
// if(num1<num2)
//   console.log("num1 is smaller than num2")

// else if(num1>num2)
//   console.log("num1 is greater")
// else
//   console.log("not sure either equal")

// function add(num1,num2)
// {
//   return num1+num2
// }

// let ans = add(5,6)
// console.log(ans)

// let greet = function(){
//   console.log("hello")
// }
// greet()
// console.log(greet())
// let greet = () => {
//   console.log("hello muskan")
// }
// greet()

// let laptop = {
//   cpu: 'i9',
//   ram: '16',
//   brand: 'HP',
//   greet: function(){
//     console.log(this.ram)
//   }
// }
// key = Object.keys(laptop)
// console.log(key)
// val = Object.values(laptop)
// console.log(val)
// laptop.greet()
// console.log(laptop.cpu)
// console.log(laptop)
// console.log(this.cpu)

// function Alien(name, tech){
//   this.name = name,
//   this.tech = tech;
//   this.work = function(){
//     console.log("solving bugs")
//   }
// }

// let alien1 = new Alien('Muskan', 'JS')
// alien1.tech = 'Blockchain'
// console.log(alien1)
// alien1.work()


// let values =[]
// values.push(5,6,89)
// values[3] = 7
// console.log(values.length)
// console.log(values)
// console.log(values.shift())
// console.log(values)

// num = 7
// num = 8
// result = num%2===0 ? "even" : "odd"
// console.log(result)

// let day ="mon"
// switch(day){
//   case 'mon':
//     console.log('7am')
//     break
//   case 'tue':
//     console.log('8am')
//   default:
//     console.log('8am')
// }

// let num1 = 9
// let num2 = 8
// let result = num1 + num2
// console.log(`the addition of ${num1} and ${num2} is ${result}.`)


// let i = 1
// while(i<=5)
// {
//   console.log("mvs")
//   i++
// }

// let i = 1
// do{
//   console.log('hi',i)
//   i++
// }while(i<=5)


// let i = 1
// for(i=1;i<=5;i++)
// {
//   console.log('hi',i)
// }

// for (i=1;i<=100;i++)
// {
//   if(i%3 ===0)
//     console.log(i)
// }

// let val1 = [1,2,3]
// let val2 = [4,5,6]
// result = val1.concat([val2])
// console.log(result)

// let num = []
// for(i=1;i<=10;i++)
// {  
//   num.push(i)
//   console.log(i)
// }

// let num = [1,2,3,4,5,6,7,9,7,8]
// for(n of num)
// console.log(n)
// for(key in nums)
// console.log(key)

// let num = [1,2,3,4,5,6,7,9,7,8]
// for(i in num)
// console.log(i*2)
// num.forEach((n) => {
//   console.log(n)
// })


// let nums = [2,67,88,45,46,78,68,66,32,43]
// nums.filter(n => n%2===0)
//     .forEach((n) => {
//       console.log(n)
//     })

// let nums = [2,67,88,45,46,78,68,66,32,43]
// console.log(nums.map(n => n%2===0))// error aaegi 

// let num = new Set("bookeeperm")
// console.log(num)

// let num = new Set()
// num.add("soni")
// num.add(1)
// console.log(num)
// num.forEach(n => {
//   console.log(n)
// })
// console.log(num.has(1))

// try{
//   let num = 10
//   num/0
// }
// catch(error){
//   console.log("not divisible"+ error)
// }

// --------------------Day2-------------
// let obj = {
//   name: "Muskan",
//   city: "Indore",
//   getIntro: function(){
//     console.log(this.name + " from " + this.city)
//   }
// }
// console.log(obj.city)

// let obj1 = {
//   name: "Anu"
// }

// obj1.__proto__ = obj
// console.log(obj1.getIntro())
// Object.getPrototypeOf(obj);


//  using create method

// let person = {
//   greet: function(){
//     console.log("hello person")
//   }
// }
// person.greet()

// let muskan = Object.create(person)
// muskan.greet()

// using constructor

// let person = {
//   greet(){
//     console.log("hello there, my name is "+ this.name)
//   }
// }
// function muskan(name){
//   this.name = name
// }

// Object.assign(muskan.prototype, person)
// let dur = new muskan("Durgesh")
// dur.greet()

// higher order function 


// function x(){
//   console.log("Hey")
// }

// function y(x){ // y here is hoigher order function and x will be callback function
//   x()
// }

// reduce function

// let num = [1,2,3,4,5]
// sum1 = (num) => {
//   let sum = 0
//   for(let i = 0; i<num.length;i++)
//     sum = sum+num[i]
//   console.log(sum)
// } 
// sum1(num)

// let num = [1,2,3,4,5]
// total_sum = num.reduce((a,b)=>{
//   a = a+b
//   return a
// })
// console.log(total_sum)



// Promise(Asynchrounus JS)

// let cart = ["shoes","top","jeans"]
// let promise = createOrder(cart)

// promise.then((orderID) =>  {
//   proceedToPayment(orderId)
// })

// let link = "https://api.gitlab.com/users/mvsoni"
// const user = fetch(link);
// console.log(user)

// promise chaining
// let cart = ["shoes","top","jeans"]
// always return the then value to avoid error
// createOrder(cart)
// .then(function (orderId){
//   return proceedToPayment(orderId)
// })
// .then(function (paymentInfo){
//   return showOrderSummary(paymentInfo)
// })
// .then(function (paymentInfo){
//   return updateWalletBalance(paymentInfo)
// })

// create a promise and handling error

// const cart = ["shoes", "top", "jeans"]
// const promise = createOrder(cart)
// promise.then((orderId) => console.log(orderId))

// function validateCart(cart) {
//   return true
// }

// function createOrder(cart) {
//   const pr = new Promise(function (resolve, reject) {
//     if (!validateCart(cart)) {
//       const err = new Error("cart is not valid")
//       reject(err)
//     }
//     const orderId = "12345"
//     if (orderId) {
//       resolve(orderId)
//     }
//   })
//   return pr
// }


// async and await
// async function findingWeather(){
//   let delhiWeather = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve("27 deg")
//     },2000)
//   })
//   let indoreWeather = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve("21 deg")
//     },7000)
//   })

//   console.log("Fetching delhi's weather........")
//   let delhiW = await delhiWeather
//   console.log("the weather of delhi is "+ delhiW)
//   console.log("now fetching indore weather please wait.....")
//   let indoreW = await indoreWeather
//   console.log("fetched indore weather "+ indoreW)
// }

// findingWeather()

//--------------Day3---------------

// const datas = [
//   {
//     name: "Muskan",
//     profession: "developer"
//   },
//   {
//     name: "Anu",
//     profession: "developer"
//   }
// ]
// function getData(datas){
//   setTimeout(() => {
//     // for(n in datas)
//     //   console.log(datas)
//     datas.forEach((n) => {
//       console.log(n.name)
//       console.log(n.profession)
//     })
//   },1000)
// }
// getData(datas)

// string methods 

// let str = "Muskan Soni"
// console.log(str.toUpperCase())
// console.log(str.toLowerCase())
// console.log(str.toLowerCase())
// console.log(str.length)
// console.log(str.includes("sonii"))
// console.log(str.startsWith("Mu"))
// console.log(str.endsWith("i"))
// console.log(str.search("n")) // returns the index of the searched word
// console.log(str.match(/n/g)) // returns the array of collected matched words.
// console.log(str.indexOf("n"))
// console.log(str.lastIndexOf("n"))
// console.log(str.replace("Muskan", "Anu"))
// console.log(str.trim()) //removes extra spaces from start and end
// console.log(str.charAt(3)) // return the character present at the given index
// console.log(str.charCodeAt(1)) // return sci-code for a string or letter
// console.log(String.fromCharCode(110)) // return a string for a [particular sci code]
// console.log(str.endsWith("i"))

// let str2 = "Vijendra"
// console.log(str.concat(str2))
// console.log(str.split(""))
// console.log(str.repeat("2")) // print number of times you want to print your string
// console.log(str.slice(3))
// console.log(str.slice(3,7))
// console.log(str.slice(-11))

// console.log(str.substr(2,5)) // count all letters on 2 on 5 and 2 to 5
// console.log(str.substring(2,5)) // same as substr but doesnt count value on 5


// template literal

// let num1 = 5
// let num2 = 9 
// let output = num1 + num2
// console.log(`the addition of ${num1} and ${num2} is ${output}`)



//promises example
// const datas = [
//   {
//     name: "Muskan",
//     profession: "developer"
//   },
//   {
//     name: "Anu",
//     profession: "developer"
//   }
// ]
// function getData(){
//   setTimeout(() => {
//     // for(n in datas)
//     //   console.log(datas)
//     datas.forEach((n) => {
//       console.log(n.name)
//       console.log(n.profession)
//     })
//   },1000)
// }

// function createData(newdata,callback){
//   setTimeout(() => {
//     datas.push(newdata)
//     callback()
//   }, 2000);
// }

//2
// createData({name: "Vijendra",profession: "employee"},getData)

// let name1 = () => {
//   setTimeout(() => {
//     console.log("your name is Muskan")
//   }, 3000)
// }

// let age = () => {
//   setTimeout(() => {
//     console.log("your age is 22")
//   },2000)
// }

// async function display(){
//   name1()
//   age()
//   console.log("end")
// }
// display()

// reverse of a string

// let str = "Muskan"
// let ans = str.split('')
// .reverse().join('')
// console.log(ans)

// sum of all elements in the array
// var num = [1,2,3,4,56,7]
// totalSum = (num) => {
//   let sum = 0
//   for(let i=0;i<num.length;i++)
//     sum = sum + num[i]
//   console.log(sum)  
// } 

// totalSum(num)

//using reduce 
// var num = [1,2,3,4,56,7]
// totalSum = num.reduce((a,b) => {
//     a = a+b
//     return a
//   })

// console.log(totalSum)

// const a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// for (let i = 0; i < 10; i++) {
//   setTimeout(() => console.log(a[i]), 1000);
// }

// for (var i = 0; i < 10; i++) {
//   setTimeout(() => console.log(a[i]), 1000);
// }
// let a = 10;
// var a = 20;
// console.log(a);

// -------------Day4 -------------------------


// var a = 15;
// console.log(this.a);
// function calling - call apply and bind
// var car = { 
//   registrationNumber: "GA12345",
//   brand: "Toyota",

//   displayDetails: function(){
//       console.log(this.registrationNumber + " " + this.brand);
//   }
// }
// car.displayDetails()

// var myCarDetails =  car.displayDetails;
// myCarDetails();

//bind 
// let car1 = {
//   registrationNumber: "MNOP9876",
//   brand: "HARRIER"
// }
// var myCarDetails = car.displayDetails.bind(car1); 
// myCarDetails();

//call and apply
// var car = { 
//   registrationNumber: "GA12345",
//   brand: "Toyota"
// }

// function displayDetails(ownerName) {
//   console.log(ownerName + ", this is your car: " + this.registrationNumber + " " + this.brand);
// }
// displayDetails(car)  // this will return undefined

// displayDetails.call(car1,'Muskan')
// displayDetails.call(car,'Muskan')
// displayDetails.apply(car,['Anu']) // it takes arguments in form of string

// closures 
// closures - function along with its lexical scope 

// function x(){
//   var a = 7
//   function y(){
//     console.log(a)
//   }
//   y()
// }
// x()

// const outerFunction = () => {
//   var x = 10
//   // console.log(y) // reference error

//   const innerFunction = () => {
//     var y = 20 
//     console.log(x)
//     if(true){
//       console.log(x)
//       console.log(y)
//     }
//   }
//   innerFunction()
// }
// outerFunction()

//callback
// function passed inside a function as an argument is called callback function

// let greet = (name, calling1) => {
//   console.log("Hi! there I am ", name)
//   calling1()
// }

// let calling = () => {
//   console.log("I am calling a callback function")
// }

// greet('Muskan', calling)

// another way

// let greet = (name, calling1) => {
//   console.log("Hi! there I am ", name,calling1())
// }

// let calling = () => {
//   return("I am calling a callback function")
// }

// greet('Muskan', calling)